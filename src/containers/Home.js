import React, { Component } from 'react';
import Todolist from '../components/Todolist';
import {STATE_INCOMPLETE} from '../stores/TaskStore';
import { inject, observer } from 'mobx-react';

import './../assets/stylesheets/Home.css';

@inject('TaskStore')
@observer
class Home extends Component {

    constructor(props) {
        super(props);

        const {TaskStore} = this.props;

        TaskStore.getAll().then((tasks) => {
            console.info(tasks.length + ' tarefas retornadas do serviço');
        })
    }

    handleTaskStateChange(state, id) {
        const {TaskStore} = this.props;
        const task = TaskStore.tasks.filter((task) => task._id === id)[0];
        task.state = state;
        TaskStore.update(task).then(() => {
            console.info('Task Atualizada');
        });
    }

    handleRemoveTask(id) {
        const {TaskStore} = this.props;
        TaskStore.remove(id).then(() => {
            console.info('Task removida');
        });
    }

    handleAddTask(taskName) {
        const {TaskStore} = this.props;
        TaskStore.insert({ name: taskName, state : STATE_INCOMPLETE}).then((data) => {
            console.info('Task adicionada');
        });
    }

    handleClearCompletedTasks() {
        const {TaskStore} = this.props;
        TaskStore.clearCompleted().then(() => {
            console.info('Tasks completas removidas');
        });
    }

    handleCheckAllTasks() {
        const {TaskStore} = this.props;
        TaskStore.checkAll().then(() => {
           console.info('Marcou todas como completas');
        });
    }

    render() {
        const {TaskStore} = this.props;

        return (
            <div className="home">
                <Todolist
                    loading={TaskStore.loading}
                    tasks={TaskStore.tasks}
                    onTaskStateChange={this.handleTaskStateChange.bind(this)}
                    onTaskRemove={this.handleRemoveTask.bind(this)}
                    onTaskAdd={this.handleAddTask.bind(this)}
                    onClearCompletedTasks={this.handleClearCompletedTasks.bind(this)}
                    onCheckAllTasks={this.handleCheckAllTasks.bind(this)}
                />
            </div>
        );
    }

}

export default Home;