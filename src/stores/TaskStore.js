import {observable, action} from 'mobx';
import Api from './../utils/Api';

export const STATE_INCOMPLETE = 'incomplete';
export const STATE_COMPLETED = 'completed';
export const STATE_ALL = 'all';

class TaskStore {

    @observable loading = false;
    @observable tasks = [];

    @action
    getAll() {
        this.loading = true;
        return Api.call('/task', 'get', {isDeleted: false}).then((data) => {
            this.loading = false;
            this.tasks = data.docs;
            return this.tasks;
        });
    }

    @action
    update(task) {
        const params = {
            name : task.name || null,
            state : task.state || null
        };

        return Api.call(`/task/${task._id}`, 'put', params).then((data) => {
            console.log(data);
        });
    }

    @action
    remove(taskId) {
        return Api.call(`/task/${taskId}`, 'delete', {}).then(() => {
            this.tasks = this.tasks.filter((task) => task._id !== taskId);
        });
    }

    insert(task) {
        return Api.call('/task', 'post', task).then((data) => {
            this.tasks.push(data);
            return data;
        })
    }

    @action
    clearCompleted() {
        return new Promise((resolve) => {
            this.tasks.map((task) => {
               if (task.state === STATE_COMPLETED) {
                   this.remove(task._id);
               }
            });
            resolve(true);
        });
    }

    @action
    checkAll() {
        return new Promise((resolve) => {
            this.tasks.map((task) => {
                if (task.state === STATE_INCOMPLETE) {
                    task.state = STATE_COMPLETED;
                    this.update(task).catch();
                }
            });
            resolve(true);
        })
    }

}

export default new TaskStore();