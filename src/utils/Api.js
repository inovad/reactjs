import axios from 'axios';

export const API_URI = "http://api.session.hero99.com.br";

class Api {

    call(url, method, params = {}) {

        if (method === 'get') {
            const getParams = [];
            Object.keys(params).map((key) => {
                getParams.push(key + '=' + params[key]);
            });
            url += '?' + getParams.join('&');
            params = {};
        }

        return axios({
            url : url,
            method : method,
            baseURL : API_URI,
            headers : {
                'Content-Type' : 'application/json'
            },
            data: params,
        }).then(function(response) {
            return response.data
        });
    }
}

export default new Api();