import React, { Component } from 'react';
import Task from '../components/Task';
import { STATE_ALL, STATE_INCOMPLETE, STATE_COMPLETED } from '../stores/TaskStore';
import './../assets/stylesheets/Todolist.css'
import {observer} from 'mobx-react';

const ENTER_KEY = 'Enter';

@observer
class Todolist extends Component {

    static defaultProps = {
        onTaskStateChange: () => {},
        onTaskRemove: () => {},
        onTaskAdd: () => {},
        onClearCompletedTasks: () => {},
        onCheckAllTasks: () => {}
    }

    constructor(props) {
        super(props);

        this.state = {
            loading: props.loading || false,
            tasks: props.tasks || [],
            tabActive: STATE_ALL
        }
    }

    componentWillReceiveProps(props) {
        this.setState({...props});
    }

    handleButtonCheckAllTasksClick() {
        this.props.onCheckAllTasks();
    }

    handleInputTaskKeyUp(event) {
        const taskName = event.target.value;
        if (event.key === ENTER_KEY) {
            this.props.onTaskAdd(taskName);
            event.target.value = '';
        }
    }

    handleClearCompletedTasks() {
        this.props.onClearCompletedTasks();
    }

    handleTabClick(tabActive) {
        this.setState({ tabActive: tabActive});
    }

    handleTaskStateChange(state, id) {
        this.props.onTaskStateChange(state, id);
    }

    handleRemoveTask(id) {
        this.props.onTaskRemove(id);
    }

    render() {
        return (
            <div className="todolist">
                <div className="top">
                    <button onClick={this.handleButtonCheckAllTasksClick.bind(this)}></button>
                    <input
                        onKeyUp={this.handleInputTaskKeyUp.bind(this)}
                        type="text"
                        placeholder="O que precisa ser feito?"
                    />
                </div>
                <div>
                    { this.state.loading ?
                        <div className="loading">Carregando...</div> :
                        this.state.tasks.map((task) => {
                            if ( (task.state === this.state.tabActive) || this.state.tabActive === STATE_ALL ) {
                                return <Task
                                    id={task._id}
                                    name={task.name}
                                    state={task.state}
                                    key={task._id}
                                    onTaskStateChange={this.handleTaskStateChange.bind(this)}
                                    onTaskRemove={this.handleRemoveTask.bind(this)}
                                />
                            }
                        })
                    }

                </div>
                <div className="bottom">
                    <div className="info">
                        { this.state.tasks.filter((task) => task.state === STATE_INCOMPLETE).length } restantes
                    </div>
                    <div className="tabs">
                        <button
                            onClick={this.handleTabClick.bind(this, STATE_ALL)}
                            className={this.state.tabActive === STATE_ALL ? "active" : ''}
                        >
                            Todas
                        </button>
                        <button
                            onClick={this.handleTabClick.bind(this, STATE_INCOMPLETE)}
                            className={this.state.tabActive === STATE_INCOMPLETE ? "active" : ''}
                        >
                            Incompletas
                        </button>
                        <button
                            onClick={this.handleTabClick.bind(this, STATE_COMPLETED)}
                            className={this.state.tabActive === STATE_COMPLETED ? "active" : ''}
                        >
                            Finalizadas
                        </button>
                    </div>
                    <div className="clear">
                        <button onClick={this.handleClearCompletedTasks.bind(this)}>Limpar finalizadas</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Todolist;