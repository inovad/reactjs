import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter as Router, Route} from "react-router-dom";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from 'mobx-react';

import Layout from "./containers/Layout";
import Home from './containers/Home';
import About from './containers/About';

import TaskStore from './stores/TaskStore';

const stores = {
    TaskStore
}

ReactDOM.render(
    <Provider {...stores}>
        <Router>
            <Layout>
                <Route exact={true} path="/" component={Home}></Route>
                <Route path="/home" component={Home}></Route>
                <Route path="/about" component={About}></Route>
            </Layout>
        </Router>
    </Provider>
    , document.getElementById('root')
);
registerServiceWorker();
