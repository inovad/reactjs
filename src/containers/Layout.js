import React, { Component } from 'react';
import '../assets/stylesheets/Layout.css';
import { Link } from 'react-router-dom';

class Layout extends Component {

    render() {
        return (
            <div className="app">
                <div className="header">
                    TodoList
                </div>
                <div className="content">
                    { this.props.children }
                </div>
                <div className="footer">
                    <Link to="/home">Home</Link> | <Link to="/about">About</Link>
                </div>
            </div>
        );
    }
}

export default Layout;