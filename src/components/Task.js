import React, { Component } from 'react';
import { STATE_INCOMPLETE, STATE_COMPLETED } from './../stores/TaskStore';
import {observer} from 'mobx-react';
import './../assets/stylesheets/Task.css';


class Task extends Component {

    static defaultProps = {
        onTaskStateChange: () => {},
        onTaskRemove: () => {}
    }

    constructor(props) {
        super(props);

        this.state = {
            state : props.state || STATE_INCOMPLETE
        }
    }

    componentWillReceiveProps(props) {
        this.setState({...props});
    }

    handleToggleCheckTask() {
        let taskState = this.state.state;

        if (taskState === STATE_INCOMPLETE) {
            taskState = STATE_COMPLETED;
        } else {
            taskState = STATE_INCOMPLETE;
        }

        this.props.onTaskStateChange(taskState, this.props.id);
    }

    handleRemoveTask() {
        this.props.onTaskRemove(this.props.id);
    }

    render() {
        return (
            <div className="task">
                <input type="checkbox" onClick={this.handleToggleCheckTask.bind(this)} checked={(this.state.state === STATE_COMPLETED) ? true : false}/>
                <p className={"name" + (this.state.state === STATE_COMPLETED ? ' checked' : '')}>{this.props.name}</p>
                <div className="remove" onClick={this.handleRemoveTask.bind(this)}>X</div>
            </div>
        );
    }
}

export default Task;